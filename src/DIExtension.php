<?php

namespace AbraD450\ConnectionManager;

use Nette;

/**
 * DI Extension
 */
class DIExtension extends Nette\DI\CompilerExtension
{

    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();
        $builder->addDefinition($this->prefix("connectionManager"))
            ->setFactory(ConnectionManager::class)
            ->setAutowired(TRUE);        
    }
    
}