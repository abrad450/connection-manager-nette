<?php

namespace AbraD450\ConnectionManager;

use Nette\DI\Container;
use Nette\Database\Connection;
use Nette\Database\Explorer;

/**
 * Connection Manager
 */
final class ConnectionManager
{

    /**
     * DI Container
     * 
     * @var Container
     */
    private $container;

    
    /**
     * Cached db connections
     * 
     * @var array
     */
    private $connections;
    
    /**
     * Cached db explorers
     * 
     * @var array
     */
    private $explorers;
    
    
    /**
     * Constructor
     * 
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Get connection by its name
     * 
     * @param string $name Connection name
     * @return Connection
     * @throws \InvalidArgumentException
     */
    public function getConnection(string $name): Connection
    {
        if(empty($this->connections)) {
            $this->connections = $this->container->findByType(Connection::class);
        }
        if(($index = array_search("database.{$name}.connection", $this->connections, TRUE)) === FALSE) {
            throw new \InvalidArgumentException("Database Connection '{$name}' not found");
        }
        return $this->container->getService($this->connections[$index]);
    }

    /**
     * Get Database Explorer by its name
     * 
     * @param string $name Explorer name
     * @return Explorer
     * @throws \InvalidArgumentException
     */
    public function getExplorer(string $name): Explorer
    {
        if(empty($this->explorers)) {
            $this->explorers = $this->container->findByType(Explorer::class);
        }
        if(($index = array_search("database.{$name}.explorer", $this->explorers, TRUE)) === FALSE) {
            throw new \InvalidArgumentException("Database Explorer '{$name}' not found");
        }
        return $this->container->getService($this->explorers[$index]);
    }
    
    
    /**
     * Get system connection
     * 
     * @return Connection
     */
    public function getSystemConnection(): Connection
    {
        return $this->getConnection('system');
    }    
    
    /**
     * Get system explorer
     * 
     * @return Explorer
     */
    public function getSystemExplorer(): Explorer
    {
        return $this->getExplorer('system');
    }

}