# Nette Database Connection Manager

## Registration

Register extension in NEON file:

	extensions:
	    connectionManager: AbraD450\ConnectionManager\DIExtension

Configure nette database connections as usual with the "system" connection being the main app connection

	database:
	    system:
	    	dsn: 'mysql:host=...;dbname=...'
	    	user: '...'
	    	password: '...'
		other:
	    	dsn: '...'
	    	user: '...'
	    	password: '...'
		otherConnection:
			.
			.
			.

Inject the service and get the db explorers like this:

    public function __construct(ConnectionManager $connectionManager)
    {
		// system explorer
        $this->db = $connectionManager->getSystemExplorer();
		// other db explorer
		$this->otherDb = $connectionManager->getExplorer('other');
    }


You can obtain DB Connections instance if needed

	// system connection
    $dbConn = $connectionManager->getSystemConnection();

	// other db connection
	$otherDbConn = $connectionManager->getConnection('other');

